import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../core/bloc/movie_bloc.dart';
import '../core/repositories/api_repository.dart';
import '../ui/home/pages/home_page.dart';

class MyApp extends StatelessWidget {
  MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return RepositoryProvider(
      create: (context) => ApiRepository(),
      child: BlocProvider(
        create: (context) => MovieBloc(context.read<ApiRepository>()),
        child: MaterialApp(
          title: 'Flutter Demo',
          // TODO implement go_router
          home: const HomePage(),
        ),
      ),
    );
  }
}
