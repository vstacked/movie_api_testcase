import 'package:json_annotation/json_annotation.dart';

import 'movie.dart';

part 'parent_response.g.dart';

@JsonSerializable()
class ParentResponse {
  int? page;
  @JsonKey(name: "total_pages")
  int? totalPages;
  List<Movie>? movies;

  ParentResponse({this.page, this.movies, this.totalPages});

  factory ParentResponse.fromJson(Map<String, dynamic> json) => _$ParentResponseFromJson(json);

  Map<String, dynamic> toJson() => _$ParentResponseToJson(this);
}
