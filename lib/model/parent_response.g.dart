// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'parent_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ParentResponse _$ParentResponseFromJson(Map<String, dynamic> json) {
  return ParentResponse(
    page: json['page'] as int?,
    movies: (json['movies'] as List<dynamic>?)
        ?.map((e) => Movie.fromJson(e as Map<String, dynamic>))
        .toList(),
    totalPages: json['total_pages'] as int?,
  );
}

Map<String, dynamic> _$ParentResponseToJson(ParentResponse instance) =>
    <String, dynamic>{
      'page': instance.page,
      'total_pages': instance.totalPages,
      'movies': instance.movies,
    };
