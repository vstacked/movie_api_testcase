// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'movie.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Movie _$MovieFromJson(Map<String, dynamic> json) {
  return Movie(
    id: json['id'] as int?,
    backdropPath: json['backdrop_path'] as String?,
    title: json['original_title'] as String?,
    overview: json['overview'] as String?,
    releaseDate: json['release_date'] as String?,
    voteAverage: (json['vote_average'] as num?)?.toDouble(),
    genres: (json['genres'] as List<dynamic>?)
        ?.map((e) => Genre.fromJson(e as Map<String, dynamic>))
        .toList(),
    homepage: json['homepage'] as String?,
    budget: json['budget'] as int?,
    trailer: json['trailer'] == null
        ? null
        : Trailer.fromJson(json['trailer'] as Map<String, dynamic>),
  )..posterPath = json['poster_path'] as String?;
}

Map<String, dynamic> _$MovieToJson(Movie instance) => <String, dynamic>{
      'id': instance.id,
      'poster_path': instance.posterPath,
      'backdrop_path': instance.backdropPath,
      'original_title': instance.title,
      'overview': instance.overview,
      'release_date': instance.releaseDate,
      'vote_average': instance.voteAverage,
      'genres': instance.genres?.map((e) => e.toJson()).toList(),
      'homepage': instance.homepage,
      'budget': instance.budget,
      'trailer': instance.trailer?.toJson(),
    };

Genre _$GenreFromJson(Map<String, dynamic> json) {
  return Genre(
    id: json['id'] as int?,
    name: json['name'] as String?,
  );
}

Map<String, dynamic> _$GenreToJson(Genre instance) => <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
    };

Trailer _$TrailerFromJson(Map<String, dynamic> json) {
  return Trailer(
    key: json['key'] as String?,
  );
}

Map<String, dynamic> _$TrailerToJson(Trailer instance) => <String, dynamic>{
      'key': instance.key,
    };
