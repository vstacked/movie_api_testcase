import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:infinite_scroll_pagination/infinite_scroll_pagination.dart';

import '../../../core/bloc/movie_bloc.dart';
import '../../../core/repositories/api_repository.dart';
import '../../../model/movie.dart';
import '../../../widgets/drawer_widget.dart';
import 'details_page.dart';

class ListPage extends StatelessWidget {
  const ListPage({
    Key? key,
    this.isNowPlaying = false,
    this.isTopRated = false,
    this.isPopular = false,
    this.isUpcoming = false,
  }) : super(key: key);
  final bool isNowPlaying;
  final bool isTopRated;
  final bool isPopular;
  final bool isUpcoming;

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => MovieBloc(context.read<ApiRepository>()),
      child: Builder(
        builder: (context) {
          if (isNowPlaying)
            return _Screen(title: 'Now Playing', isNowPlaying: true);
          else if (isTopRated)
            return _Screen(title: 'Top Rated', isTopRated: true);
          else if (isPopular)
            return _Screen(title: 'Popular', isPopular: true);
          else if (isUpcoming) return _Screen(title: 'Upcoming', isUpcoming: true);
          return Scaffold(body: const Text('Error Occurred'));
        },
      ),
    );
  }
}

class _Screen extends StatefulWidget {
  final String title;
  final bool isNowPlaying;
  final bool isTopRated;
  final bool isPopular;
  final bool isUpcoming;

  const _Screen({
    Key? key,
    required this.title,
    this.isNowPlaying = false,
    this.isTopRated = false,
    this.isPopular = false,
    this.isUpcoming = false,
  }) : super(key: key);
  @override
  _ScreenState createState() => _ScreenState();
}

class _ScreenState extends State<_Screen> {
  final PagingController<int, Movie> _pagingController = PagingController(firstPageKey: 1);

  @override
  void initState() {
    super.initState();
    _pagingController.addPageRequestListener(
      (pageKey) {
        final bloc = context.read<MovieBloc>();
        if (widget.isNowPlaying)
          bloc.add(FetchNowPlaying(page: pageKey));
        else if (widget.isTopRated)
          bloc.add(FetchTopRated(page: pageKey));
        else if (widget.isPopular)
          bloc.add(FetchPopular(page: pageKey));
        else if (widget.isUpcoming) bloc.add(FetchUpcoming(page: pageKey));
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<MovieBloc, MovieState>(
      listener: (context, state) {
        if (state is MovieLoaded) {
          if (state.parent.page! <= state.parent.totalPages!) {
            _pagingController.appendPage(state.parent.movies!, state.parent.page! + 1);
          } else {
            _pagingController.appendLastPage([]);
          }
        } else if (state is MovieError) {
          _pagingController.error = state.message;
        }
      },
      builder: (context, state) {
        return Scaffold(
          appBar: AppBar(
            leading: Builder(
              builder: (context) => IconButton(
                onPressed: () => Scaffold.of(context).openDrawer(),
                icon: const Icon(Icons.menu),
              ),
            ),
            title: Text(widget.title),
          ),
          drawer: DrawerWidget(
            isNowPlaying: widget.isNowPlaying,
            isPopular: widget.isPopular,
            isTopRated: widget.isTopRated,
            isUpcoming: widget.isUpcoming,
          ),
          body: PagedListView<int, Movie>(
            pagingController: _pagingController,
            builderDelegate: PagedChildBuilderDelegate<Movie>(
              itemBuilder: (context, item, index) => Padding(
                padding: const EdgeInsets.all(8.0),
                child: ListTile(
                  leading: Container(
                    width: 75,
                    decoration: BoxDecoration(
                      image: DecorationImage(
                        image: NetworkImage('https://image.tmdb.org/t/p/original${item.posterPath}'),
                        fit: BoxFit.cover,
                      ),
                      borderRadius: BorderRadius.circular(5),
                    ),
                  ),
                  onTap: () =>
                      Navigator.push(context, MaterialPageRoute(builder: (context) => DetailsPage(id: item.id!))),
                  title: Text(item.title ?? '-'),
                  trailing: Text(item.voteAverage.toString(), style: Theme.of(context).textTheme.subtitle2),
                ),
              ),
            ),
          ),
        );
      },
    );
  }

  @override
  void dispose() {
    _pagingController.dispose();
    super.dispose();
  }
}
