import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_youtube_view/flutter_youtube_view.dart';
import 'package:intl/intl.dart';
import 'package:url_launcher/url_launcher.dart';

import '../../../core/bloc/movie_bloc.dart';
import '../../../core/repositories/api_repository.dart';

class DetailsPage extends StatelessWidget {
  const DetailsPage({Key? key, required this.id}) : super(key: key);
  final int id;

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => MovieBloc(context.read<ApiRepository>())..add(FetchDetail(id: id)),
      child: _Screen(),
    );
  }
}

class _Screen extends StatelessWidget {
  const _Screen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<MovieBloc, MovieState>(
      builder: (context, state) {
        if (state is MovieDetailLoaded) {
          return Scaffold(
            body: CustomScrollView(
              slivers: [
                SliverAppBar(
                  backgroundColor: Color(0xFFEDF2F8),
                  expandedHeight: 300,
                  flexibleSpace: FlexibleSpaceBar(
                    background: Image.network(
                      'https://image.tmdb.org/t/p/original${state.movie.backdropPath}',
                      fit: BoxFit.cover,
                    ),
                    centerTitle: true,
                    title: Text(state.movie.title ?? ''),
                  ),
                ),
                SliverToBoxAdapter(
                  child: Card(
                    child: Column(
                      children: [
                        const SizedBox(height: 15),
                        Column(
                          children: <Widget>[
                            Text(
                              'Trailer',
                              style: Theme.of(context).textTheme.subtitle1!.copyWith(fontWeight: FontWeight.w600),
                            ),
                            if (state.movie.trailer != null)
                              SizedBox(
                                height: 250,
                                child: FlutterYoutubeView(
                                  onViewCreated: (controller) => controller,
                                  params: YoutubeParam(videoId: state.movie.trailer!.key!, autoPlay: false),
                                ),
                              ),
                          ],
                        ),
                        const SizedBox(height: 15),
                        Column(
                          children: <Widget>[
                            Text(
                              'Overview',
                              style: Theme.of(context).textTheme.subtitle1!.copyWith(fontWeight: FontWeight.w600),
                            ),
                            Text(state.movie.overview ?? '-'),
                          ],
                        ),
                        const SizedBox(height: 15),
                        Column(
                          children: <Widget>[
                            Text(
                              'Release Date',
                              style: Theme.of(context).textTheme.subtitle1!.copyWith(fontWeight: FontWeight.w600),
                            ),
                            Text(getDate(state.movie.releaseDate ?? '')),
                          ],
                        ),
                        const SizedBox(height: 15),
                        Column(
                          children: <Widget>[
                            Text(
                              'Vote Average',
                              style: Theme.of(context).textTheme.subtitle1!.copyWith(fontWeight: FontWeight.w600),
                            ),
                            Text(state.movie.voteAverage.toString()),
                          ],
                        ),
                        const SizedBox(height: 15),
                        Column(
                          children: <Widget>[
                            Text(
                              'Budget',
                              style: Theme.of(context).textTheme.subtitle1!.copyWith(fontWeight: FontWeight.w600),
                            ),
                            Text(NumberFormat.currency().format(state.movie.budget)),
                          ],
                        ),
                        const SizedBox(height: 15),
                        Column(
                          children: <Widget>[
                            Text(
                              'Homepage',
                              style: Theme.of(context).textTheme.subtitle1!.copyWith(fontWeight: FontWeight.w600),
                            ),
                            TextButton(
                              onPressed: () {
                                if (state.movie.homepage != null) launch(state.movie.homepage!);
                              },
                              child: Text(state.movie.homepage ?? '-'),
                            ),
                          ],
                        ),
                        const SizedBox(height: 15),
                      ],
                    ),
                  ),
                )
              ],
            ),
          );
        }
        return Scaffold(body: Center(child: CircularProgressIndicator()));
      },
    );
  }

  String getDate(String date) {
    if (date.isEmpty) return '-';
    final List<int> split = date.split('-').map((e) => int.parse(e)).toList();
    return DateFormat.yMMMMd().format(DateTime(split[0], split[1], split[2]));
  }
}
