import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';

import '../../../core/bloc/movie_bloc.dart';
import '../../../core/repositories/api_repository.dart';
import '../../../model/movie.dart';
import '../../../widgets/drawer_widget.dart';
import '../widgets/api_search_delegate.dart';
import 'details_page.dart';
import 'list_page.dart';

class HomePage extends StatelessWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: Builder(
          builder: (context) => IconButton(
            onPressed: () => Scaffold.of(context).openDrawer(),
            icon: const Icon(Icons.menu),
          ),
        ),
        title: const Text('Home'),
        actions: [
          IconButton(
            onPressed: () => showSearch<Movie?>(context: context, delegate: ApiSearchDelegate()),
            icon: const Icon(Icons.search),
          ),
        ],
      ),
      drawer: DrawerWidget(isHome: true),
      body: SingleChildScrollView(
        child: Column(
          children: [
            BlocProvider(
              create: (_) => MovieBloc(context.read<ApiRepository>())..add(FetchNowPlaying(isInitial: true)),
              child: BlocBuilder<MovieBloc, MovieState>(
                builder: (context, state) {
                  return _MovieCard(
                    title: 'Now Playing',
                    onMorePressed: () =>
                        Navigator.push(context, MaterialPageRoute(builder: (_) => ListPage(isNowPlaying: true))),
                    movies: state is MovieLoaded ? state.parent.movies ?? [] : [],
                    isLoading: state is MovieLoading,
                    isError: state is MovieError,
                    errorMessage: state is MovieError ? state.message : '',
                    onTryAgainPressed: () {
                      context.read<MovieBloc>().add(FetchNowPlaying(isInitial: true));
                    },
                  );
                },
              ),
            ),
            BlocProvider(
              create: (_) => MovieBloc(context.read<ApiRepository>())..add(FetchTopRated(isInitial: true)),
              child: BlocBuilder<MovieBloc, MovieState>(
                builder: (context, state) {
                  return _MovieCard(
                    title: 'Top Rated',
                    onMorePressed: () =>
                        Navigator.push(context, MaterialPageRoute(builder: (_) => ListPage(isTopRated: true))),
                    movies: state is MovieLoaded ? state.parent.movies ?? [] : [],
                    isLoading: state is MovieLoading,
                    isError: state is MovieError,
                    errorMessage: state is MovieError ? state.message : '',
                    onTryAgainPressed: () {
                      context.read<MovieBloc>().add(FetchTopRated(isInitial: true));
                    },
                  );
                },
              ),
            ),
            BlocProvider(
              create: (_) => MovieBloc(context.read<ApiRepository>())..add(FetchPopular(isInitial: true)),
              child: BlocBuilder<MovieBloc, MovieState>(
                builder: (context, state) {
                  return _MovieCard(
                    title: 'Popular',
                    onMorePressed: () =>
                        Navigator.push(context, MaterialPageRoute(builder: (_) => ListPage(isPopular: true))),
                    movies: state is MovieLoaded ? state.parent.movies ?? [] : [],
                    isLoading: state is MovieLoading,
                    isError: state is MovieError,
                    errorMessage: state is MovieError ? state.message : '',
                    onTryAgainPressed: () {
                      context.read<MovieBloc>().add(FetchPopular(isInitial: true));
                    },
                  );
                },
              ),
            ),
            BlocProvider(
              create: (_) => MovieBloc(context.read<ApiRepository>())..add(FetchUpcoming(isInitial: true)),
              child: BlocBuilder<MovieBloc, MovieState>(
                builder: (context, state) {
                  return _MovieCard(
                    title: 'Upcoming',
                    onMorePressed: () =>
                        Navigator.push(context, MaterialPageRoute(builder: (_) => ListPage(isUpcoming: true))),
                    movies: state is MovieLoaded ? state.parent.movies ?? [] : [],
                    isLoading: state is MovieLoading,
                    isError: state is MovieError,
                    errorMessage: state is MovieError ? state.message : '',
                    onTryAgainPressed: () {
                      context.read<MovieBloc>().add(FetchUpcoming(isInitial: true));
                    },
                  );
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class _MovieCard extends StatelessWidget {
  const _MovieCard({
    Key? key,
    required this.title,
    required this.onMorePressed,
    required this.movies,
    this.isLoading = false,
    this.isError = false,
    this.errorMessage = '',
    this.onTryAgainPressed,
  }) : super(key: key);
  final String title;
  final VoidCallback onMorePressed;
  final List<Movie> movies;
  final bool isLoading;
  final bool isError;
  final VoidCallback? onTryAgainPressed;
  final String errorMessage;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 8.0),
      child: Column(
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 16.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text(title, style: Theme.of(context).textTheme.headline6),
                TextButton(onPressed: onMorePressed, child: const Text('MORE')),
              ],
            ),
          ),
          SizedBox(
            height: 250,
            child: Builder(
              builder: (context) {
                if (isError) {
                  return Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Flexible(
                        child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Text(
                            errorMessage,
                            style: Theme.of(context).textTheme.subtitle1!.copyWith(color: Colors.red),
                            maxLines: 3,
                            overflow: TextOverflow.ellipsis,
                          ),
                        ),
                      ),
                      TextButton(onPressed: onTryAgainPressed, child: const Text('Try Again')),
                    ],
                  );
                }
                if (!isLoading) {
                  return ListView.builder(
                    itemCount: movies.length,
                    scrollDirection: Axis.horizontal,
                    shrinkWrap: true,
                    itemBuilder: (BuildContext context, int index) {
                      final movie = movies.elementAt(index);
                      return GestureDetector(
                        onTap: () {
                          Navigator.push(context, MaterialPageRoute(builder: (context) => DetailsPage(id: movie.id!)));
                        },
                        child: SizedBox(
                          width: 175,
                          child: Card(
                            shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
                            child: Column(
                              children: <Widget>[
                                Container(
                                  height: 180,
                                  width: 175,
                                  decoration: BoxDecoration(
                                    image: DecorationImage(
                                      image: NetworkImage('https://image.tmdb.org/t/p/original${movie.posterPath}'),
                                      fit: BoxFit.cover,
                                    ),
                                    borderRadius:
                                        BorderRadius.only(topLeft: Radius.circular(10), topRight: Radius.circular(10)),
                                  ),
                                ),
                                Expanded(
                                  child: Padding(
                                    padding: const EdgeInsets.all(4.0),
                                    child: Column(
                                      mainAxisAlignment: MainAxisAlignment.center,
                                      children: <Widget>[
                                        Text(movie.title ?? '-', maxLines: 1, overflow: TextOverflow.ellipsis),
                                        Text(getDate(movie.releaseDate ?? '')),
                                      ],
                                    ),
                                  ),
                                )
                              ],
                            ),
                          ),
                        ),
                      );
                    },
                  );
                }
                return const Center(child: CircularProgressIndicator());
              },
            ),
          ),
        ],
      ),
    );
  }

  String getDate(String date) {
    if (date.isEmpty) return '-';
    final List<int> split = date.split('-').map((e) => int.parse(e)).toList();
    return DateFormat.yMMMMd().format(DateTime(split[0], split[1], split[2]));
  }
}
