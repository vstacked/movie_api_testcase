import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:infinite_scroll_pagination/infinite_scroll_pagination.dart';
import 'package:rxdart/rxdart.dart';

import '../../../core/bloc/movie_bloc.dart';
import '../../../model/movie.dart';
import '../pages/details_page.dart';

class ApiSearchDelegate extends SearchDelegate<Movie?> {
  @override
  Widget buildLeading(BuildContext context) => IconButton(
        tooltip: 'Back',
        icon: AnimatedIcon(icon: AnimatedIcons.menu_arrow, progress: transitionAnimation),
        onPressed: () => close(context, null),
      );

  @override
  List<Widget> buildActions(BuildContext context) {
    return query.isEmpty
        ? []
        : <Widget>[
            IconButton(
              tooltip: 'Clear',
              icon: const Icon(Icons.clear),
              onPressed: () {
                query = '';
                showSuggestions(context);
              },
            )
          ];
  }

  @override
  Widget buildResults(BuildContext context) {
    if (query.isEmpty) return const SizedBox();
    return _ItemSearch(query: query);
  }

  @override
  Widget buildSuggestions(BuildContext context) {
    if (query.isEmpty) return const SizedBox();
    return _ItemSearch(query: query);
  }
}

class _ItemSearch extends StatefulWidget {
  const _ItemSearch({Key? key, required this.query}) : super(key: key);
  final String query;

  @override
  __ItemSearchState createState() => __ItemSearchState();
}

class __ItemSearchState extends State<_ItemSearch> {
  final PagingController<int, Movie> _pagingController = PagingController(firstPageKey: 1);
  final _searchTerms = BehaviorSubject<String>();

  @override
  void initState() {
    super.initState();
    _pagingController.addPageRequestListener((pageKey) {
      context.read<MovieBloc>().add(SearchMovie(query: widget.query, page: pageKey));
    });
    _searchTerms.debounce((event) => TimerStream(true, const Duration(milliseconds: 500))).listen((value) {
      context.read<MovieBloc>().add(SearchMovie(query: value));
      _pagingController.refresh();
    });
  }

  @override
  void didUpdateWidget(covariant _ItemSearch oldWidget) {
    super.didUpdateWidget(oldWidget);
    if (widget.query != oldWidget.query) _searchTerms.add(widget.query);
  }

  @override
  void dispose() {
    _pagingController.dispose();
    _searchTerms.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<MovieBloc, MovieState>(
      listener: (context, state) {
        if (state is MovieLoaded) {
          if (state.parent.page! <= state.parent.totalPages!) {
            _pagingController.appendPage(state.parent.movies!, state.parent.page! + 1);
          } else {
            _pagingController.appendLastPage([]);
          }
        } else if (state is MovieError) {
          _pagingController.error = state.message;
        }
      },
      builder: (context, state) {
        return PagedListView<int, Movie>(
          pagingController: _pagingController,
          builderDelegate: PagedChildBuilderDelegate<Movie>(
            itemBuilder: (context, item, index) => Padding(
              padding: const EdgeInsets.all(8.0),
              child: ListTile(
                leading: Container(
                  width: 75,
                  decoration: BoxDecoration(
                    image: DecorationImage(
                      image: NetworkImage('https://image.tmdb.org/t/p/original${item.posterPath}'),
                      fit: BoxFit.cover,
                    ),
                    borderRadius: BorderRadius.circular(5),
                  ),
                ),
                onTap: () =>
                    Navigator.push(context, MaterialPageRoute(builder: (context) => DetailsPage(id: item.id!))),
                title: Text(item.title ?? '-'),
                trailing: Text(item.voteAverage.toString(), style: Theme.of(context).textTheme.subtitle2),
              ),
            ),
          ),
        );
      },
    );
  }
}
