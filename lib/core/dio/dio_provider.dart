import 'package:dio/dio.dart';

import '../util/env.dart';

abstract class DioProvider {
  BaseOptions _options = BaseOptions(
    baseUrl: 'https://api.themoviedb.org/3',
    connectTimeout: 12000,
    receiveTimeout: 12000,
    queryParameters: {'api_key': Env.apiKey},
  );

  late Dio dio = Dio(_options);
}
