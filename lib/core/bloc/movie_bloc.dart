import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';

import '../../model/movie.dart';
import '../../model/parent_response.dart';
import '../repositories/api_repository.dart';

part 'movie_event.dart';
part 'movie_state.dart';

class MovieBloc extends Bloc<MovieEvent, MovieState> {
  final ApiRepository _repository;
  MovieBloc(this._repository) : super(MovieInitial()) {
    on<FetchNowPlaying>((event, emit) async {
      if (event.isInitial) emit(MovieLoading());

      final data = await _repository.getNowPlaying(page: event.page);
      data.fold(
        (l) => emit(MovieLoaded(l)),
        (r) => emit(MovieError(r)),
      );
    });
    on<FetchTopRated>((event, emit) async {
      if (event.isInitial) emit(MovieLoading());

      final data = await _repository.getTopRated(page: event.page);
      data.fold(
        (l) => emit(MovieLoaded(l)),
        (r) => emit(MovieError(r)),
      );
    });
    on<FetchPopular>((event, emit) async {
      if (event.isInitial) emit(MovieLoading());

      final data = await _repository.getPopular(page: event.page);
      data.fold(
        (l) => emit(MovieLoaded(l)),
        (r) => emit(MovieError(r)),
      );
    });
    on<FetchUpcoming>((event, emit) async {
      if (event.isInitial) emit(MovieLoading());

      final data = await _repository.getUpcoming(page: event.page);
      data.fold(
        (l) => emit(MovieLoaded(l)),
        (r) => emit(MovieError(r)),
      );
    });
    on<FetchDetail>((event, emit) async {
      emit(MovieDetailLoading());

      final data = await _repository.getDetails(event.id);
      data.fold(
        (l) => emit(MovieDetailLoaded(l)),
        (r) => emit(MovieDetailError(r)),
      );
    });
    on<SearchMovie>((event, emit) async {
      emit(MovieLoading());

      final data = await _repository.search(event.query, page: event.page);
      data.fold(
        (l) => emit(MovieLoaded(l)),
        (r) => emit(MovieError(r)),
      );
    });
  }
}
