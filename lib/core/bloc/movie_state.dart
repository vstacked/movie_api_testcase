part of 'movie_bloc.dart';

abstract class MovieState extends Equatable {
  const MovieState();

  @override
  List<Object> get props => [];
}

class MovieInitial extends MovieState {}

class MovieLoading extends MovieState {}

class MovieLoaded extends MovieState {
  final ParentResponse parent;

  const MovieLoaded(this.parent);

  @override
  List<Object> get props => [parent];
}

class MovieError extends MovieState {
  final String message;

  const MovieError(this.message);

  @override
  List<Object> get props => [message];
}

class MovieDetailLoading extends MovieState {}

class MovieDetailLoaded extends MovieState {
  final Movie movie;

  const MovieDetailLoaded(this.movie);

  @override
  List<Object> get props => [movie];
}

class MovieDetailError extends MovieState {
  final String message;

  const MovieDetailError(this.message);

  @override
  List<Object> get props => [message];
}
