part of 'movie_bloc.dart';

abstract class MovieEvent extends Equatable {
  const MovieEvent();

  @override
  List<Object> get props => [];
}

class FetchNowPlaying extends MovieEvent {
  final bool isInitial;
  final int page;
  FetchNowPlaying({this.isInitial = false, this.page = 1});

  @override
  List<Object> get props => [page];
}

class FetchTopRated extends MovieEvent {
  final bool isInitial;
  final int page;
  FetchTopRated({this.isInitial = false, this.page = 1});

  @override
  List<Object> get props => [page];
}

class FetchPopular extends MovieEvent {
  final bool isInitial;
  final int page;
  FetchPopular({this.isInitial = false, this.page = 1});

  @override
  List<Object> get props => [page];
}

class FetchUpcoming extends MovieEvent {
  final bool isInitial;
  final int page;
  FetchUpcoming({this.isInitial = false, this.page = 1});

  @override
  List<Object> get props => [page];
}

class FetchDetail extends MovieEvent {
  final int id;
  FetchDetail({required this.id});

  @override
  List<Object> get props => [id];
}

class SearchMovie extends MovieEvent {
  final String query;
  final int page;
  SearchMovie({required this.query, this.page = 1});

  @override
  List<Object> get props => [query, page];
}
