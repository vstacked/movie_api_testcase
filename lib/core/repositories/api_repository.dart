import 'package:dartz/dartz.dart';
import 'package:dio/dio.dart';

import '../../model/movie.dart';
import '../../model/parent_response.dart';
import '../dio/dio_provider.dart';

class ApiRepository extends DioProvider implements _IApiRepository {
  @override
  Future<Either<ParentResponse, String>> getNowPlaying({int page = 0}) async {
    try {
      final response = await dio.get('/movie/now_playing', queryParameters: {'page': page});

      return Left(
        ParentResponse.fromJson(response.data)
          ..movies = (response.data['results'] as List).map((e) => Movie.fromJson(e)).toList(),
      );
    } on DioError catch (e) {
      return Right(e.message);
    } catch (e) {
      return Right('Error Occurred');
    }
  }

  @override
  Future<Either<Movie, String>> getDetails(int id) async {
    try {
      final response = await dio.get('/movie/$id');
      final trailer = await getTrailer(id);
      return trailer.fold(
        (l) => Left(Movie.fromJson(response.data)..trailer = l),
        (r) => Left(Movie.fromJson(response.data)),
      );
    } on DioError catch (e) {
      return Right(e.message);
    } catch (e) {
      return Right('Error Occurred');
    }
  }

  @override
  Future<Either<Trailer, String>> getTrailer(int id) async {
    try {
      final response = await dio.get('/movie/$id/videos');

      return Left(Trailer.fromJson((response.data['results'] as List).where((e) => e['type'] == 'Trailer').first));
    } on DioError catch (e) {
      return Right(e.message);
    } catch (e) {
      return Right('Error Occurred');
    }
  }

  @override
  Future<Either<ParentResponse, String>> getPopular({int page = 0}) async {
    try {
      final response = await dio.get('/movie/popular', queryParameters: {'page': page});

      return Left(
        ParentResponse.fromJson(response.data)
          ..movies = (response.data['results'] as List).map((e) => Movie.fromJson(e)).toList(),
      );
    } on DioError catch (e) {
      return Right(e.message);
    } catch (e) {
      return Right('Error Occurred');
    }
  }

  @override
  Future<Either<ParentResponse, String>> getTopRated({int page = 0}) async {
    try {
      final response = await dio.get('/movie/top_rated', queryParameters: {'page': page});

      return Left(
        ParentResponse.fromJson(response.data)
          ..movies = (response.data['results'] as List).map((e) => Movie.fromJson(e)).toList(),
      );
    } on DioError catch (e) {
      return Right(e.message);
    } catch (e) {
      return Right('Error Occurred');
    }
  }

  @override
  Future<Either<ParentResponse, String>> getUpcoming({int page = 0}) async {
    try {
      final response = await dio.get('/movie/upcoming', queryParameters: {'page': page});

      return Left(
        ParentResponse.fromJson(response.data)
          ..movies = (response.data['results'] as List).map((e) => Movie.fromJson(e)).toList(),
      );
    } on DioError catch (e) {
      return Right(e.message);
    } catch (e) {
      return Right('Error Occurred');
    }
  }

  @override
  Future<Either<ParentResponse, String>> search(String query, {int page = 0}) async {
    try {
      final response = await dio.get('/search/movie', queryParameters: {'query': query, 'page': page});

      return Left(
        ParentResponse.fromJson(response.data)
          ..movies = (response.data['results'] as List).map((e) => Movie.fromJson(e)).toList(),
      );
    } on DioError catch (e) {
      return Right(e.message);
    } catch (e) {
      return Right('Error Occurred');
    }
  }
}

abstract class _IApiRepository {
  Future<Either<ParentResponse, String>> getNowPlaying({int page = 0});
  Future<Either<ParentResponse, String>> getTopRated({int page = 0});
  Future<Either<ParentResponse, String>> getPopular({int page = 0});
  Future<Either<ParentResponse, String>> getUpcoming({int page = 0});
  Future<Either<Movie, String>> getDetails(int id);
  Future<Either<Trailer, String>> getTrailer(int id);
  Future<Either<ParentResponse, String>> search(String search, {int page = 0});
}
