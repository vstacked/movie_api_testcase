import 'package:flutter/material.dart';

import '../ui/home/pages/home_page.dart';
import '../ui/home/pages/list_page.dart';

class DrawerWidget extends StatelessWidget {
  const DrawerWidget({
    Key? key,
    this.isHome = false,
    this.isNowPlaying = false,
    this.isTopRated = false,
    this.isPopular = false,
    this.isUpcoming = false,
  }) : super(key: key);
  final bool isHome;
  final bool isNowPlaying;
  final bool isTopRated;
  final bool isPopular;
  final bool isUpcoming;

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        padding: EdgeInsets.zero,
        children: <Widget>[
          DrawerHeader(
            margin: EdgeInsets.zero,
            padding: EdgeInsets.zero,
            decoration: BoxDecoration(color: Colors.blue),
            child: Center(
              child: Text(
                'Movie API\'s',
                style:
                    Theme.of(context).textTheme.headline5!.copyWith(color: Colors.white, fontWeight: FontWeight.w600),
              ),
            ),
          ),
          createDrawerBodyItem(isHome, icon: Icons.home, text: 'Home', onTap: () => push(context, page: HomePage())),
          createDrawerBodyItem(
            isNowPlaying,
            icon: Icons.play_circle_fill_outlined,
            text: 'Now Playing',
            onTap: () => push(context, page: ListPage(isNowPlaying: true)),
          ),
          createDrawerBodyItem(
            isTopRated,
            icon: Icons.auto_graph_sharp,
            text: 'Top Rated',
            onTap: () => push(context, page: ListPage(isTopRated: true)),
          ),
          createDrawerBodyItem(
            isPopular,
            icon: Icons.star_border_purple500_outlined,
            text: 'Popular',
            onTap: () => push(context, page: ListPage(isPopular: true)),
          ),
          createDrawerBodyItem(
            isUpcoming,
            icon: Icons.playlist_play_outlined,
            text: 'Upcoming',
            onTap: () => push(context, page: ListPage(isUpcoming: true)),
          ),
        ],
      ),
    );
  }

  void push(BuildContext context, {required Widget page}) {
    Navigator.pop(context);
    if (isHome) {
      Navigator.push(context, MaterialPageRoute(builder: (context) => page));
    } else {
      Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => page));
    }
  }

  Widget createDrawerBodyItem(bool isActive,
      {required IconData icon, required String text, required GestureTapCallback onTap}) {
    return ListTile(
      selected: isActive,
      title: Row(children: <Widget>[Icon(icon), Padding(padding: const EdgeInsets.only(left: 8.0), child: Text(text))]),
      onTap: onTap,
    );
  }
}
